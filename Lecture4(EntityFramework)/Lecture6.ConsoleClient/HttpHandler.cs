﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Net.Http.Json;
using System.Text;
using System.Threading.Tasks;
using Lecture4.Models.DTOs;
using Lecture4.Models.AuxiliaryModels;

namespace Lecture6.ConsoleClient
{
    public class HttpHandler
    {
        private readonly HttpClient _client;

        public HttpHandler()
        {
            _client = new HttpClient();
            _client.BaseAddress = new Uri("https://localhost:44369/");
            _client.DefaultRequestHeaders.Accept.Clear();
            _client.DefaultRequestHeaders.Accept.Add(
                new MediaTypeWithQualityHeaderValue("application/json"));
        }

        public async Task<List<TaskDTO>> GetNotFinishedTasks()
        {
            var action = $"api/tasks/getnotfinishedtasks";

            HttpResponseMessage response = await _client.GetAsync(action);
            if (response.IsSuccessStatusCode)
            {
                var tasks = await response.Content.ReadFromJsonAsync<List<TaskDTO>>();
                return tasks;
            }

            throw new Exception("Error");
        }

        public async Task<int> MarkTaskAsFinished(int id)
        {
            var action = $"api/tasks/getnotfinishedtasks/{id}";

            HttpResponseMessage response = await _client.GetAsync(action);
            if (response.IsSuccessStatusCode)
            {
                return await response.Content.ReadFromJsonAsync<int>();
            }

            throw new Exception("Error");
        }

        public async Task<Dictionary<ProjectDTO, int>> GetDictionary(int id)
        {
            var action = $"api/linq/getdictionary{id}";

            HttpResponseMessage response = await _client.GetAsync(action);
            if (response.IsSuccessStatusCode)
            {
                var dict = await response.Content.ReadFromJsonAsync<Dictionary<ProjectDTO, int>>();
                return dict;
            }

            throw new Exception("Error");
        }

        public async Task<List<TaskDTO>> GetListOfTasksByUserId(int id)
        {
            var action = $"api/linq/gettasksforuser/{id}";

            HttpResponseMessage response = await _client.GetAsync(action);
            if (response.IsSuccessStatusCode)
            {
                var list = await response.Content.ReadFromJsonAsync<List<TaskDTO>>();
                return list;
            }

            throw new Exception("Error");
        }

        public async Task<List<TaskIdAndName>> GetTasksFinishedIn2021(int id)
        {
            var action = $"api/linq/gettasksfinishedin2021byuser/{id}";

            HttpResponseMessage response = await _client.GetAsync(action);
            if (response.IsSuccessStatusCode)
            {
                var list = await response.Content.ReadFromJsonAsync<List<TaskIdAndName>>();
                return list;
            }

            throw new Exception("Error");
        }

        public async Task<List<TeamIdNameAndUsers>> Task4Method()
        {
            var action = $"api/linq/getteamswhereallusersolderthan10";

            HttpResponseMessage response = await _client.GetAsync(action);
            if (response.IsSuccessStatusCode)
            {
                var list = await response.Content.ReadFromJsonAsync<List<TeamIdNameAndUsers>>();
                return list;
            }

            throw new Exception("Error");
        }

        public async Task<List<UserNameAndSortedTasks>> GetSortedUsers()
        {
            var action = $"api/linq/getsorteduserswithsortedtasks";

            HttpResponseMessage response = await _client.GetAsync(action);
            if (response.IsSuccessStatusCode)
            {
                var list = await response.Content.ReadFromJsonAsync<List<UserNameAndSortedTasks>>();
                return list;
            }

            throw new Exception("Error");
        }

        public async Task<UserAndInfoAboutHisTasksAndProjects> Task6Method(int id)
        {
            var action = $"api/linq/getuserandinfoabouthistasksandprojects/{id}";

            HttpResponseMessage response = await _client.GetAsync(action);
            if (response.IsSuccessStatusCode)
            {
                var list = await response.Content.ReadFromJsonAsync<UserAndInfoAboutHisTasksAndProjects>();
                return list;
            }

            throw new Exception("Error");
        }

        public async Task<List<ProjectAndInfoAboutItsTasksAndCustomers>> Task7Method()
        {
            var action = $"api/linq/getprojectandinfoaboutitstasksandcustomers";

            HttpResponseMessage response = await _client.GetAsync(action);
            if (response.IsSuccessStatusCode)
            {
                var list = await response.Content.ReadFromJsonAsync<List<ProjectAndInfoAboutItsTasksAndCustomers>>();
                return list;
            }

            throw new Exception("Error");
        }

    }
}
