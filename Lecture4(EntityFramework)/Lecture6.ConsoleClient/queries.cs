﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using Lecture4.Models.DTOs;

namespace Lecture6.ConsoleClient
{
    public class queries
    {
        private readonly HttpHandler _handler = new HttpHandler();

        public Task<int> MarkRandomTaskWithDelay(int milliSeconds)
        {
            var tcs = new TaskCompletionSource<int>();

            var backgroundWorker = new BackgroundWorker();
            backgroundWorker.DoWork += (o, e) =>
            {
                Task.Delay(milliSeconds);
                var tasks = _handler.GetNotFinishedTasks().Result;

                Random rand = new Random();

                if (tasks.Count == 0)
                    tcs.SetException(new Exception("All tasks are finished"));

                int randomIndex = rand.Next(tasks.Count);
                var taskToMark = tasks[randomIndex];

                var markedTaskId = _handler.MarkTaskAsFinished(taskToMark.Id).Result;
                tcs.SetResult(markedTaskId);
            };

            backgroundWorker.RunWorkerCompleted += (o, e) =>
            {
                if (e.Error != null)
                {
                    tcs.SetException(e.Error);
                }
                else
                {
                    tcs.SetResult((int)e.Result);
                }
            };

            return tcs.Task;
        }
    }
}
