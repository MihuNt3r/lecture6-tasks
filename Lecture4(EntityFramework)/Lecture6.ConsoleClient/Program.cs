﻿using System;
using System.Threading.Tasks;

namespace Lecture6.ConsoleClient
{
    class Program
    {
        static async Task Main(string[] args)
        {
            Menu mainMenu = new Menu();
            await mainMenu.Start();
        }
    }
}
