﻿using System;
using System.Collections.Generic;

namespace Lecture4.Models.Models
{
    public class Team
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public DateTime CreatedAt { get; set; }
        public List<User> Customers {get; set;}
        public Team()
        {
            Customers = new List<User>();
        }
    }
}
