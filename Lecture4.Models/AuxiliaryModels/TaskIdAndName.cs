﻿namespace Lecture4.Models.AuxiliaryModels
{
    public class TaskIdAndName
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
