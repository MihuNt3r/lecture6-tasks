﻿using System;
using System.Linq;
using System.Collections.Generic;
using Lecture4.DataAccess;
using Lecture4.Models.Models;
using Lecture4.Models.DTOs;
using Task = System.Threading.Tasks.Task;
using TaskEntity = Lecture4.Models.Models.Task;
using Microsoft.EntityFrameworkCore;
using System.Threading.Tasks;
using AutoMapper;

namespace Lecture4.Services
{
    public class TaskService
    {
        private readonly MyDbContext _context;
        private readonly IMapper _mapper;

        public TaskService(MyDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        public async Task<IEnumerable<TaskDTO>> GetAllAsync()
        {
            var tasks = await _context.Tasks.ToListAsync();

            List<TaskDTO> taskDTOs = _mapper.Map<List<TaskDTO>>(tasks);

            return taskDTOs;
        }

        public async Task<TaskDTO> GetByIdAsync(int id)
        {
            if (id < 0)
                throw new ArgumentException("Wrong id");

            var task = await _context.Tasks.FindAsync(id);

            if (task == null)
                throw new Exception("Can't find task with such id");

            TaskDTO taskDTO = _mapper.Map<TaskDTO>(task);

            return taskDTO;
        }

        public async Task<List<TaskDTO>> GetAllNotFinishedTasksAsync()
        {
            var tasks = await _context.Tasks.ToListAsync();

            var notFinishedTasks = (from t in tasks
                                    where t.FinishedAt == null
                                    select _mapper.Map<TaskDTO>(t)).ToList();

            return notFinishedTasks;
        }

        public async Task MarkTaskAsFinishedAsync(int id)
        {
            if (id < 0)
                throw new ArgumentException("Wrong id");

            var task = await _context.Tasks.FindAsync(id);

            if (task == null)
                throw new Exception("Can't find task with such id");

            if (task.FinishedAt != null)
                throw new ArgumentException("Task is already finished");

            task.FinishedAt = DateTime.Now;

            _context.Update(task);
            await _context.SaveChangesAsync();
        }

        public async Task AddAsync(TaskDTO taskDTO)
        {
            if (taskDTO.Id < 0)
                throw new ArgumentException("Wrong id");

            var task = _mapper.Map<TaskEntity>(taskDTO);

            await _context.Tasks.AddAsync(task);
            await _context.SaveChangesAsync();
        }

        public async Task UpdateAsync(TaskDTO taskDTO)
        {
            var task = _mapper.Map<TaskEntity>(taskDTO);

            _context.Update(task);
            await _context.SaveChangesAsync();
        }

        public async Task DeleteAsync(int id)
        {
            if (id < 0)
                throw new ArgumentException("Wrong id");

            var task = await _context.Tasks.FindAsync(id);

            if (task == null)
                throw new Exception("Can't find task with such id");

            _context.Tasks.Remove(task);
            await _context.SaveChangesAsync();
        }
    }
}
