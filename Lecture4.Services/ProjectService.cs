﻿using System;
using System.Linq;
using System.Collections.Generic;
using Lecture4.DataAccess;
using Lecture4.Models.Models;
using Lecture4.Models.DTOs;
using Task = System.Threading.Tasks.Task;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using AutoMapper;

namespace Lecture4.Services
{
    public class ProjectService
    {
        private readonly MyDbContext _context;
        private readonly IMapper _mapper;

        public ProjectService(MyDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        public async Task<IEnumerable<ProjectDTO>> GetAllAsync()
        {
            var projects = await _context.Projects.ToListAsync();

            List<ProjectDTO> projectDTOs = _mapper.Map<List<ProjectDTO>>(projects);

            return projectDTOs;
        }

        public async Task<ProjectDTO> GetByIdAsync(int id)
        {
            if (id < 0)
                throw new ArgumentException("Wrong id");

            var project = await _context.Projects.FindAsync(id);

            if (project == null)
                throw new Exception("Can't find project with such id");

            ProjectDTO projectDTO = _mapper.Map<ProjectDTO>(project);

            return projectDTO;
        }

        public async Task AddAsync(ProjectDTO projectDTO)
        {
            if (projectDTO.Id < 0)
                throw new ArgumentException("Wrong id");

            var project = _mapper.Map<Project>(projectDTO);

            await _context.Projects.AddAsync(project);
            await _context.SaveChangesAsync();
        }

        public async Task UpdateAsync(ProjectDTO projectDTO)
        {
            var project = _mapper.Map<Project>(projectDTO);

            _context.Update(project);
            await _context.SaveChangesAsync();
        }

        public async Task DeleteAsync(int id)
        {
            if (id < 0)
                throw new ArgumentException("Wrong id");

            var project = await _context.Projects.FindAsync(id);

            if (project == null)
                throw new Exception("Can't find project with such id");

            _context.Projects.Remove(project);
            await _context.SaveChangesAsync();
        }
    }
}
