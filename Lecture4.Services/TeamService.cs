﻿using System;
using System.Linq;
using System.Collections.Generic;
using Lecture4.DataAccess;
using Lecture4.Models.Models;
using Lecture4.Models.DTOs;
using Task = System.Threading.Tasks.Task;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using AutoMapper;

namespace Lecture4.Services
{
    public class TeamService
    {
        private readonly MyDbContext _context;
        private readonly IMapper _mapper;

        public TeamService(MyDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        public async Task<IEnumerable<TeamDTO>> GetAllAsync()
        {
            var teams = await _context.Teams.ToListAsync();

            List<TeamDTO> teamDTOs = _mapper.Map<List<TeamDTO>>(teams);

            return teamDTOs;
        }

        public async Task<TeamDTO> GetByIdAsync(int id)
        {
            if (id < 0)
                throw new ArgumentException("Wrong id");

            var team = await _context.Teams.FindAsync(id);

            if (team == null)
                throw new Exception("Can't find team with such id");

            TeamDTO teamDTO = _mapper.Map<TeamDTO>(team);

            return teamDTO;
        }

        public async Task AddAsync(TeamDTO teamDTO)
        {
            if (teamDTO.Id < 0)
                throw new ArgumentException("Wrong id");

            var team = _mapper.Map<Team>(teamDTO);

            await _context.Teams.AddAsync(team);
            await _context.SaveChangesAsync();
        }

        public async Task UpdateAsync(TeamDTO teamDTO)
        {
            var team = _mapper.Map<Team>(teamDTO);

            _context.Update(team);
            await _context.SaveChangesAsync();
        }

        public async Task DeleteAsync(int id)
        {
            if (id < 0)
                throw new ArgumentException("Wrong id");

            var team = await _context.Teams.FindAsync(id);

            if (team == null)
                throw new Exception("Can't find team with such id");

            _context.Teams.Remove(team);
            await _context.SaveChangesAsync();
        }
    }
}
